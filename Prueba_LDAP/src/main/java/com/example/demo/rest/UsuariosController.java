package com.example.demo.rest;

import java.util.Optional;

import javax.naming.InvalidNameException;
import javax.naming.ldap.LdapName;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.User;
import com.example.demo.service.UserService;

@RestController()
public class UsuariosController {

	@Autowired
	UserService userService;

	@GetMapping()
	public String hello() {
		userService.showUsers();
		return "Bienvenido, en consola estan todos los usuarios";
	}

	/*
	 * Como LDAP es un directorio, se divide en varias unidades organizativas o grupos, 
	 * por lo tanto, el metodo debe recibir dos parametros: 
	 * 1) El nombre delgrupo donde buscara el usuario(OU) 
	 * 2) El nombre de usuario(UID)
	 */
	@GetMapping("buscar/{group}/{username}")
	public String buscar(@PathVariable("group") String group, @PathVariable("username") String username)
			throws InvalidNameException {
		LdapName id = null;
		id = new LdapName("uid=" + username + ",ou=" + group);
		Optional<User> user = userService.findUser(id);
		if (user.isPresent()) {
			System.out.println("CN: " + user.get().getCommonName());
			System.out.println("SN: " + user.get().getSuerName());
			System.out.println("DN - UO: " + user.get().getDn());
			System.out.println("UID: " + user.get().getUID());
			return "Busqueda exitosa de " + username;
		} else {
			System.out.println(username + " en grupo " + group + " no encontrado");
			return "Usuario no encontrado";
		}
	}
}