package com.example.demo.service;

import java.util.Optional;

import javax.naming.Name;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.model.User;
import com.example.demo.repository.UserRepository;

@Service
public class UserService {
	@Autowired
	UserRepository userRepository;
	
	public void showUsers() {
		userRepository.findAll().forEach(p -> {
			System.out.println(p.toString() + "\n");
		});
	}
	
	/* Busca el usuario por el DN */
	public Optional<User> findUser(Name uid) {
		return userRepository.findById(uid);
	}
}
